
draft-tiloca-ace-oscore-gm-admin-00.xml: draft-tiloca-ace-oscore-gm-admin.xml
	sed -e '$(join $(addprefix s/,$(addsuffix -latest/,$(drafts))), $(addsuffix /g;,$(drafts_next)))' $< > $@
diff-draft-tiloca-ace-oscore-gm-admin-.txt.html: draft-tiloca-ace-oscore-gm-admin-.txt draft-tiloca-ace-oscore-gm-admin.txt
	-$(rfcdiff) --html --stdout $^ > $@
